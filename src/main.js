import Vue from 'vue';
import VueRouter from 'vue-router'
import VueResource from 'vue-resource';

import PageHome from './scripts/components/PageHome.vue';
import PageAboutSite from './scripts/components/PageAboutSite.vue';

Vue.use(VueRouter);
Vue.use(VueResource);

const router = new VueRouter({
    routes: [
        {path: '/', name: 'home', component: PageHome},
        {path: '/about', name: 'about', component: PageAboutSite},
    ],
    linkExactActiveClass: 'active',
});

const App = new Vue({
    router,
    el: '#app',
});

